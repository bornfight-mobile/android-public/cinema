package com.bornfight.utils

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class DataMap(val map: HashMap<String, String> = hashMapOf()) : Parcelable, MutableMap<String, String> by map