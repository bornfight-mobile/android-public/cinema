package com.bornfight.utils

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import com.google.android.material.dialog.MaterialAlertDialogBuilder

object DialogUtil {

    fun buildInfoDialog(context: Context, title: String?, description: String?, buttonText: String?): Dialog {
        return MaterialAlertDialogBuilder(context).apply {
            setTitle(title)
            setMessage(description)
            buttonText?.let { setPositiveButton(it) { dialog: DialogInterface?, _: Int -> dialog?.dismiss() } }
        }.create()
    }
}