package com.bornfight.utils

import com.bornfight.cinemaApp.main.model.Genre
import java.util.*

object GenreUtil {

    private var genreList: MutableList<Genre> = ArrayList()

    fun setGenres(genres: List<Genre>) {
        genreList.clear()
        genreList.addAll(genres)
    }

    fun getGenreName(genreId: Int): String? {
        for (genre in genreList) {
            if (genreId == genre.id) {
                return genre.name.toString()
            }
        }
        return null
    }

    fun isListEmpty(): Boolean = genreList.isEmpty()
}