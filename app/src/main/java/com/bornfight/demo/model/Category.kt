package com.bornfight.demo.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Category(
    @SerializedName("id")
    @PrimaryKey
    var id: Long = 0,
    @SerializedName("feelingLucky")
    var feelingLucky: Int = 0,
    @SerializedName("categoryAny")
    var feelingLuckyAnyChoice: String = "",
    @SerializedName("feeling_lucky_title")
    var feelingLuckyTitle: String = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("gradient_color_start")
    var gradientColorStart: String? = null,
    @SerializedName("gradient_color_end")
    var gradientColorEnd: String? = null
)