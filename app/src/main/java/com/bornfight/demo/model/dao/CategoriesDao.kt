package com.bornfight.demo.model.dao

import androidx.room.*
import com.bornfight.demo.model.Category
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoriesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(category: Category)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategories(categories: List<Category>)

    @Update
    suspend fun updateCategory(category: Category)

    @Delete
    suspend fun deleteCategory(category: Category)

    @Query("delete from Category where id = :categoryId")
    suspend fun deleteFromCategoryById(categoryId: Long)

    @Query("delete from Category")
    suspend fun deleteAllCategories()

    @Query("select * from Category")
    fun observeCategories(): Flow<List<Category>>

    @Query("select * from Category where id = :id")
    fun observeCategory(id: Long): Flow<Category>

    @Query("select * from Category")
    suspend fun getCategories(): List<Category>

    @Query("select * from Category where id = :id")
    suspend fun getCategory(id: Long): Category

}