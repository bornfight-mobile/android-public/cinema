package com.bornfight.demo.model.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bornfight.common.mvvm.model.dao.BaseRemoteKeyDao
import com.bornfight.demo.model.*
import kotlinx.coroutines.flow.Flow

@Dao
interface NewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNewsList(news: List<News>)

    @Query("SELECT * FROM News ORDER BY News.createdAt DESC")
    fun observeNewsList(): Flow<List<News>>

    @Query("SELECT * FROM News ORDER BY News.createdAt DESC")
    fun observeNewsPaginated(): PagingSource<Int, News>

    @Query("DELETE FROM News")
    fun deleteNewsItems(): Int

}

@Dao
interface NewsRemoteKeyDao : BaseRemoteKeyDao<NewsRemoteKeys> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insertAll(remoteKey: List<NewsRemoteKeys>)

    @Query("SELECT * FROM NewsRemoteKeys WHERE newsId = :itemId")
    override fun remoteKeysByItemId(itemId: Long): NewsRemoteKeys?

    @Query("DELETE FROM NewsRemoteKeys")
    override fun clearRemoteKeys()
}