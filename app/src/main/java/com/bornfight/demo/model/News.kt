package com.bornfight.demo.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bornfight.common.mvvm.model.BaseRemoteKeys
import com.google.gson.annotations.SerializedName

@Entity
data class News(
    @SerializedName("id")
    @PrimaryKey
    var id: Long = 0,

    @SerializedName("title")
    var title: String = "",

    @SerializedName("image_url")
    var imageUrl: String? = "",

    @SerializedName("created_at")
    var createdAt: Long? = 0
)

@Entity
data class NewsRemoteKeys(
    @PrimaryKey val newsId: Long,
    override val prevKey: Int?,
    override val nextKey: Int?
) : BaseRemoteKeys