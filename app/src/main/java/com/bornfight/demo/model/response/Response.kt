package com.bornfight.demo.model.response

import com.google.gson.annotations.SerializedName

data class Response<T>(
    @SerializedName("data")
    val data: T
)