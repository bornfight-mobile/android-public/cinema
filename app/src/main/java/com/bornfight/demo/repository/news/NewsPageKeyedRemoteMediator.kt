package com.bornfight.demo.repository.news

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingState
import androidx.room.withTransaction
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.mvvm.BasePageKeyedRemoteMediator
import com.bornfight.common.mvvm.model.dao.BaseRemoteKeyDao
import com.bornfight.demo.model.News
import com.bornfight.demo.model.NewsRemoteKeys

/**
 * Make sure to have the same sort from DB as it is from the backend side, otherwise items get mixed up and prevKey
 * and nextKey are no longer valid (the scroll might get stuck or the load might loop one of the pages)
 */
@OptIn(ExperimentalPagingApi::class)
class NewsPageKeyedRemoteMediator(
    initialPage: Int = 1,
    private val db: AppDatabase,
    remote: suspend (Int, Int) -> List<News>
) : BasePageKeyedRemoteMediator<News, NewsRemoteKeys>(
    initialPage,
    db,
    db.newsRemoteKeysDao(),
    remote
) {

    override fun mapResponseToKeys(item: News, prevKey: Int?, nextKey: Int?): NewsRemoteKeys {
        return NewsRemoteKeys(newsId = item.id, prevKey = prevKey, nextKey = nextKey)
    }

    override fun clearDataFromDb() {
        db.newsRemoteKeysDao().clearRemoteKeys()
        db.newsDao().deleteNewsItems()
    }

    override suspend fun insertDataToDb(data: List<News>, keys: List<NewsRemoteKeys>) {
        db.newsRemoteKeysDao().insertAll(keys)
        db.newsDao().insertNewsList(data)
    }

    override fun getItemId(item: News): Long = item.id
}