package com.bornfight.demo.repository.news

import androidx.paging.Pager
import com.bornfight.common.mvvm.model.Result
import com.bornfight.demo.model.News
import kotlinx.coroutines.flow.Flow

interface NewsRepo {

    fun observeNewsList(): Flow<List<News>>

    suspend fun fetchNewsList(): Flow<Result<List<News>>>

    fun observeNewsListPaginated(): Pager<Int, News>

}