package com.bornfight.demo.repository.news

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvvm.BaseRepo
import com.bornfight.common.mvvm.RemotePagingSource
import com.bornfight.common.mvvm.model.Result
import com.bornfight.demo.model.News
import com.bornfight.demo.model.dao.NewsDao
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

private const val NEWS_PAGE_SIZE = 20

class NewsRepoImpl @Inject constructor(
    private val database: AppDatabase,
    private val apiInterface: ApiInterface
) : BaseRepo(), NewsRepo {

    private val newsDao: NewsDao = database.newsDao()

    /**
     * Function to observe data directly from database
     */
    override fun observeNewsList(): Flow<List<News>> {
        return newsDao.observeNewsList()
    }

    /**
     * Function to fetch / refresh data from remote source and save it locally.
     * By saving it locally, [observeNewsList] will trigger an update.
     */
    override suspend fun fetchNewsList(): Flow<Result<List<News>>> {
        return fetchData(
            { apiInterface.getNews(null, null, 20, 1).data },
            { newsDao.insertNewsList(it) }
        )
    }


    /**
     * Simple example of data pagination.
     * [NewsPageKeyedRemoteMediator] should observe the data from database and load the appropriate page of data
     * for remote source.
     *
     * [apiInterface] data should be sorted as in local source
     *
     * Error handing is not solved for this.
     */
    @ExperimentalPagingApi
    override fun observeNewsListPaginated(): Pager<Int, News> {
        return Pager(
            config = PagingConfig(NEWS_PAGE_SIZE, enablePlaceholders = true),
            remoteMediator = NewsPageKeyedRemoteMediator(1, database) { perPage, page ->
                apiInterface.getNews(
                    null,
                    null,
                    perPage,
                    page
                ).data.sortedByDescending { it.createdAt }
            }
        ) {
            newsDao.observeNewsPaginated()
        }
    }

    /**
     * Example paging only from local
     * */
    private fun observeLocalPaginated(): Pager<Int, News> {
        return Pager(config = PagingConfig(NEWS_PAGE_SIZE, enablePlaceholders = true)) {
            newsDao.observeNewsPaginated()
        }
    }

    /**
     * Example paging only from remote
     * */
    private fun observeRemotePaginated(): Pager<Int, News> {
        return Pager(config = PagingConfig(NEWS_PAGE_SIZE, enablePlaceholders = true)) {
            RemotePagingSource { perPage, page ->
                apiInterface.getNews(null, null, perPage, page).data
            }
        }
    }
}