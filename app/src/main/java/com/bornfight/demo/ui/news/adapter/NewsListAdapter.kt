package com.bornfight.demo.ui.news.adapter

import android.view.View
import com.bornfight.cinemaApp.R
import com.bornfight.demo.model.News
import com.bornfight.utils.adapters.GenericAdapter
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_news.view.*

class NewsAdapter : GenericAdapter<News>() {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_news
    }

    override fun getViewHolder(view: View, viewType: Int): GenericViewHolder<News> {
        return NewsViewHolder(view)
    }

    inner class NewsViewHolder(itemView: View) : GenericViewHolder<News>(itemView) {
        override fun bind(data: News) {
            Glide.with(itemView.newsImageIV.context).load(data.imageUrl)
                .into(itemView.newsImageIV)
            itemView.newsTitleTV.text = data.title
        }
    }

}