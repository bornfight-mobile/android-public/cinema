package com.bornfight.demo.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bornfight.cinemaApp.databinding.FragmentNewsBinding
import com.bornfight.common.mvvm.EventObserver
import com.bornfight.common.mvvm.model.render
import com.bornfight.common.mvvm.ui.BaseDiffUtil
import com.bornfight.common.mvvm.ui.BaseFragment
import com.bornfight.demo.ui.news.adapter.NewsAdapter
import com.bornfight.demo.ui.news.adapter.NewsPagingAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.flow.collect

private const val TAG = "NewsFragment"

@AndroidEntryPoint
class NewsFragment : BaseFragment() {

    private val viewModel: NewsViewModel by viewModels()

    // This property is only valid between onCreateView and onDestroyView.
    private var _binding: FragmentNewsBinding? = null
    private val binding get() = _binding!!

    private val newsAdapter: NewsAdapter = NewsAdapter()

    // example of paginated list
    private val newsPagingAdapter: NewsPagingAdapter =
        NewsPagingAdapter(BaseDiffUtil(
            areItemsSame = { oldNews, newNews ->
                oldNews.id == newNews.id
            },
            areContentsSame = { old, new ->
                old.title == new.title
            }
        ))

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNewsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        bind()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun bind() {
        viewModel.generalEvents.observe(viewLifecycleOwner, EventObserver { it.render(this) })
      //  viewModel.news.observe(viewLifecycleOwner, Observer { newsAdapter.setItems(it ?: listOf()) })

        // example of paginated list listener
        lifecycleScope.launchWhenCreated {
            viewModel.newsPaginated.collect {
                newsPagingAdapter.submitData(it)
            }
        }
    }

    private fun setupUI() {
        binding.newsRV.apply {
            adapter = newsPagingAdapter
            layoutManager = LinearLayoutManager(context)
        }

        refresh.setOnClickListener { viewModel.refreshNews() }
    }

}