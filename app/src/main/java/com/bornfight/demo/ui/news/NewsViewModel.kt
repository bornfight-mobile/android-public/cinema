package com.bornfight.demo.ui.news

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bornfight.common.mvvm.BaseViewModel
import com.bornfight.demo.model.News
import com.bornfight.demo.repository.news.NewsRepo

class NewsViewModel @ViewModelInject constructor(
    private val newsRepo: NewsRepo
) : BaseViewModel() {

    //
    private val triggerRefreshNews = MutableLiveData<Boolean>(true)

    // link the data to consume in the View
    val news: LiveData<List<News>> =
        observeData(
            localObserve = { newsRepo.observeNewsList() },
            remoteFetch = { newsRepo.fetchNewsList() },
            triggerRefresh = triggerRefreshNews
        )

    val newsPaginated = newsRepo.observeNewsListPaginated().flow

    fun refreshNews() {
        triggerRefreshNews.value = true
    }
}