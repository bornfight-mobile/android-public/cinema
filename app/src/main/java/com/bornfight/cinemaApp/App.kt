package com.bornfight.cinemaApp

import android.app.Application
import com.bornfight.common.logging.ReleaseTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // Init timber
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }

        // enable for Notification Channels
        // NotificationChannelsHelper.initChannels(this)
    }

}