package com.bornfight.cinemaApp.main.repository.movies

import com.bornfight.cinemaApp.main.model.Genre
import com.bornfight.cinemaApp.main.model.InTheaters
import com.bornfight.cinemaApp.main.model.RecommendedMovie
import com.bornfight.cinemaApp.main.model.UpcomingMovie
import com.bornfight.common.mvvm.model.Result
import kotlinx.coroutines.flow.Flow

interface MainRepo {

    fun observeInTheatersList(): Flow<List<InTheaters>>

    fun observeRecommendedMovieList(): Flow<List<RecommendedMovie>>

    fun observeUpcomingMovieList(): Flow<List<UpcomingMovie>>

    fun observeGenres(): Flow<List<Genre>>

    suspend fun fetchInTheatersList(): Flow<Result<List<InTheaters>?>>

    suspend fun fetchRecommendedMovieList(): Flow<Result<List<RecommendedMovie>?>>

    suspend fun fetchUpcomingMovieList(): Flow<Result<List<UpcomingMovie>?>>

    suspend fun fetchGenres(): Flow<Result<List<Genre>?>>

}