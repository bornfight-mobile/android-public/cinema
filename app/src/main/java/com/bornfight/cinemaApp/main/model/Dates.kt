package com.bornfight.cinemaApp.main.model

import androidx.room.Entity

@Entity
data class Dates(
    var id: Int = 0,
    var maximum: String? = null,
    var minimum: String? = null,
)