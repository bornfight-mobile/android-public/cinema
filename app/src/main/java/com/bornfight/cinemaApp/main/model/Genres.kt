package com.bornfight.cinemaApp.main.model

data class Genres(
    var id: Int? = null,
    var genres: List<Genre>? = null
)