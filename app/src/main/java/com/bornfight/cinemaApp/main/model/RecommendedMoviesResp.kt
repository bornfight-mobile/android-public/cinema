package com.bornfight.cinemaApp.main.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class RecommendedMoviesResp(
    var id: Int? = 0,
    var page: Int? = null,
    @SerializedName("results")
    var recommendedMovies: List<RecommendedMovie>? = null,
    var totalPages: Int? = null,
    var totalResults: Int? = null
)
