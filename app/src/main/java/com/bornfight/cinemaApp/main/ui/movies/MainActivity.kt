package com.bornfight.cinemaApp.main.ui.movies

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.viewModels
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager2.widget.ViewPager2.PageTransformer
import com.bornfight.cinemaApp.R
import com.bornfight.cinemaApp.databinding.ActivityMainBinding
import com.bornfight.cinemaApp.main.ui.movies.GenericMoviesAdapter.Companion.MovieType
import com.bornfight.cinemaApp.main.ui.movies.recommended.SliderTransformer
import com.bornfight.cinemaApp.main.ui.movies.stories.StoriesAdapter
import com.bornfight.cinemaApp.main.ui.movies.theaters.GridSpacingItemDecoration
import com.bornfight.cinemaApp.main.ui.movies.theaters.InTheatersAdapter
import com.bornfight.cinemaApp.main.ui.movies.upcoming.PagerItemDecoration
import com.bornfight.cinemaApp.main.ui.movies.upcoming.UpcomingMovieChangedCallback
import com.bornfight.common.mvvm.ui.BaseActivity
import com.bornfight.utils.GenreUtil
import dagger.hilt.android.AndroidEntryPoint

private const val SPAN_COUNT = 2

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val viewModel: MainViewModel by viewModels()
    private val storiesAdapter by lazy { StoriesAdapter() }
    private val recommendedMoviesAdapter by lazy { GenericMoviesAdapter(this, MovieType.RecommendedMovie) }
    private val upcomingMoviesAdapter by lazy { GenericMoviesAdapter(this, MovieType.UpcomingMovie) }
    private val inTheatersAdapter by lazy { InTheatersAdapter() }
    private val upcomingMovieChangedCallback by lazy { UpcomingMovieChangedCallback(binding, upcomingMoviesAdapter) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(binding.root)
        setupUI()
        bind()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_theaters, menu)
        val item = menu.findItem(R.id.spinner)
        val spinner = item.actionView as Spinner
        val adapter = ArrayAdapter.createFromResource(this, R.array.theaters, R.layout.item_spinner)
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        spinner.background.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(android.R.color.white, BlendModeCompat.SRC_ATOP)
        spinner.adapter = adapter
        spinner.clipToPadding = false
        spinner.clipChildren = false
        return true
    }

    private fun setupUI() {
        title = null
        binding.storiesRV.adapter = storiesAdapter
        binding.recommendedMoviesVP.adapter = recommendedMoviesAdapter
        binding.recommendedMoviesVP.offscreenPageLimit = 3
        binding.recommendedMoviesVP.setPageTransformer(SliderTransformer(binding.recommendedMoviesVP.offscreenPageLimit))
        binding.upcomingMoviesVP.adapter = upcomingMoviesAdapter
        binding.upcomingMoviesVP.offscreenPageLimit = 3
        binding.upcomingMoviesVP.registerOnPageChangeCallback(upcomingMovieChangedCallback)
        val nextItemVisiblePx = resources.getDimension(R.dimen.viewpager_next_item_visible)  //50dp
        val currentItemHorizontalMarginPx = resources.getDimension(R.dimen.viewpager_current_item_horizontal_margin_right)  //230dp
        val pageTranslationX = nextItemVisiblePx + currentItemHorizontalMarginPx
        val pageTransformer = PageTransformer { page: View, position: Float ->
            page.translationX = -pageTranslationX * position

            if (upcomingMovieChangedCallback.goingLeft) {
                ((page as ViewGroup).getChildAt(0) as MotionLayout).setTransition(R.id.leftToRight)
            } else {
                ((page as ViewGroup).getChildAt(0) as MotionLayout).setTransition(R.id.rightToLeft)
            }
            (page.getChildAt(0) as MotionLayout).progress = upcomingMovieChangedCallback.progress
        }
        binding.upcomingMoviesVP.setPageTransformer(pageTransformer)
        val itemDecoration = PagerItemDecoration(
            this,
            R.dimen.viewpager_current_item_horizontal_margin_left, //36dp
            R.dimen.viewpager_current_item_horizontal_margin_right  //230dp
        )
        binding.upcomingMoviesVP.addItemDecoration(itemDecoration)
        binding.inTheatersRV.adapter = inTheatersAdapter
        binding.inTheatersRV.layoutManager = GridLayoutManager(this, SPAN_COUNT)
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spacing) //10dp
        binding.inTheatersRV.addItemDecoration(GridSpacingItemDecoration(SPAN_COUNT, spacingInPixels))
    }

    private fun bind() {
        viewModel.recommendedMovies.observe(this, {
            it?.let { stories -> storiesAdapter.setItems(stories) }
            it?.let { recommendedMovies -> recommendedMoviesAdapter.setItems(recommendedMovies) }
        })
        viewModel.upcomingMovies.observe(this, {
            it?.let { upcomingMovies -> upcomingMoviesAdapter.setItems(upcomingMovies) }
            binding.upcomingMoviesVP.setCurrentItem(3, true)
        })
        viewModel.inTheaters.observe(this, { it?.let { inTheaters -> inTheatersAdapter.setItems(inTheaters) } })
        viewModel.genres.observe(this, { it?.let { genres -> GenreUtil.setGenres(genres) } })
    }
}
