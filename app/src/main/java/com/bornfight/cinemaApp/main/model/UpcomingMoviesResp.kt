package com.bornfight.cinemaApp.main.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class UpcomingMoviesResp(
    var id: Int? = null,
    var page: Int? = null,
    @SerializedName("results")
    var upcomingMovies: List<UpcomingMovie>? = null,
    var dates: Dates? = null,
    var totalPages: Int? = null,
    var totalResults: Int? = null
)