package com.bornfight.cinemaApp.main.ui.movies.recommended

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bornfight.cinemaApp.BuildConfig
import com.bornfight.cinemaApp.databinding.ItemRecommendedMovieBinding
import com.bornfight.cinemaApp.main.model.Movie
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class RecommendedMovieFragment : Fragment() {

    private val recommendedMovie: Movie? by lazy { requireArguments().getParcelable(RECOMMENDED_MOVIE) }
    private var _binding: ItemRecommendedMovieBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ItemRecommendedMovieBinding.inflate(inflater, container, false)
        Glide.with(binding.recommendedMovieIV.context)
            .load(BuildConfig.BASE_POSTER_URL + recommendedMovie?.posterPath)
            .apply(RequestOptions().transform(RoundedCorners(120)))
            .into(binding.recommendedMovieIV)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        private const val RECOMMENDED_MOVIE = "RECOMMENDED_MOVIE"

        fun newInstance(recommendedMovie: Movie): Fragment {
            return RecommendedMovieFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(RECOMMENDED_MOVIE, recommendedMovie)
                }
            }
        }
    }
}

