package com.bornfight.cinemaApp.main.model

import android.os.Parcelable
import androidx.room.Entity
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class UpcomingMovie : Movie(), Parcelable