package com.bornfight.cinemaApp.main.repository.movies

import com.bornfight.cinemaApp.BuildConfig
import com.bornfight.cinemaApp.main.model.*
import com.bornfight.cinemaApp.main.model.dao.MoviesDao
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.data.retrofit.ApiInterface
import com.bornfight.common.mvvm.BaseRepo
import com.bornfight.common.mvvm.model.Result
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class MainRepoImpl @Inject constructor(
    private val database: AppDatabase,
    private val apiInterface: ApiInterface
) : BaseRepo(), MainRepo {

    private val moviesDao: MoviesDao = database.moviesDao()

    override fun observeInTheatersList(): Flow<List<InTheaters>> {
        return moviesDao.observeInTheatresList()
    }

    override fun observeRecommendedMovieList(): Flow<List<RecommendedMovie>> {
        return moviesDao.observeMovieList()
    }

    override fun observeUpcomingMovieList(): Flow<List<UpcomingMovie>> {
        return moviesDao.observeUpcomingMovieList()
    }

    override fun observeGenres(): Flow<List<Genre>> {
        return moviesDao.observeGenres()
    }

    /**
     * Function to fetch / refresh data from remote source and save it locally.
     * By saving it locally, [observeNewsList] will trigger an update.
     */

    override suspend fun fetchInTheatersList(): Flow<Result<List<InTheaters>?>> {
        return fetchData(
            { apiInterface.getMoviesInTheaters(BuildConfig.API_KEY).inTheaters },
            { it?.let { inTheaters -> moviesDao.insertInTheatersList(inTheaters) } }
        )
    }

    override suspend fun fetchRecommendedMovieList(): Flow<Result<List<RecommendedMovie>?>> {
        return fetchData(
            { apiInterface.getRecommendedMovies(BuildConfig.API_KEY).recommendedMovies },
            { it?.let { recommendedMovies -> moviesDao.insertRecommendedMovieList(recommendedMovies) } }
        )
    }

    override suspend fun fetchUpcomingMovieList(): Flow<Result<List<UpcomingMovie>?>> {
        return fetchData(
            { apiInterface.getUpcomingMovies(BuildConfig.API_KEY).upcomingMovies },
            { it?.let { upcomingMovies -> moviesDao.insertUpcomingMovieList(upcomingMovies) } }
        )
    }

    override suspend fun fetchGenres(): Flow<Result<List<Genre>?>> {
        return fetchData(
            { apiInterface.getGenres(BuildConfig.API_KEY).genres },
            { it?.let { genres -> moviesDao.insertGenres(genres) } }
        )
    }
}