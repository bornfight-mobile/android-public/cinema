package com.bornfight.cinemaApp.main.ui.movies

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bornfight.cinemaApp.main.model.Movie
import com.bornfight.cinemaApp.main.ui.movies.recommended.RecommendedMovieFragment
import com.bornfight.cinemaApp.main.ui.movies.upcoming.UpcomingMovieFragment
import java.util.*

class GenericMoviesAdapter(fragmentManager: FragmentActivity, private val movieType: MovieType) : FragmentStateAdapter(fragmentManager) {

    private var movieList: MutableList<Movie> = ArrayList()

    override fun getItemCount() = movieList.size

    override fun createFragment(position: Int): Fragment =
        when (movieType) {
            MovieType.RecommendedMovie -> RecommendedMovieFragment.newInstance(movieList[position])
            MovieType.UpcomingMovie -> UpcomingMovieFragment.newInstance(movieList[position])
        }

    fun setItems(items: List<Movie>) {
        movieList.clear()
        movieList.addAll(items)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): Movie {
        return movieList[position]
    }

    fun isListEmpty(): Boolean = movieList.isEmpty()

    companion object {
        enum class MovieType(val id: Int) {
            RecommendedMovie(1),
            UpcomingMovie(2)
        }
    }
}