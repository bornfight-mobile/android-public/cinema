package com.bornfight.cinemaApp.main.ui.movies.upcoming

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView

class PagerItemDecoration(context: Context, @DimenRes horizontalMarginLeftInDp: Int, @DimenRes horizontalMarginRightInDp: Int) : RecyclerView.ItemDecoration() {

    private val horizontalMarginLeftInPx = context.resources.getDimension(horizontalMarginLeftInDp).toInt()
    private val horizontalMarginRightInPx = context.resources.getDimension(horizontalMarginRightInDp).toInt()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.right = horizontalMarginRightInPx
        outRect.left = horizontalMarginLeftInPx
    }
}