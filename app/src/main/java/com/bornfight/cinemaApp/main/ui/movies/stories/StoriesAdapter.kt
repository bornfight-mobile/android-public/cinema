package com.bornfight.cinemaApp.main.ui.movies.stories

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bornfight.cinemaApp.BuildConfig
import com.bornfight.cinemaApp.databinding.ItemStoryBinding
import com.bornfight.cinemaApp.main.model.Movie
import com.bornfight.utils.adapters.GenericAdapter2
import com.bumptech.glide.Glide

class StoriesAdapter : GenericAdapter2<Movie, ItemStoryBinding>() {

    override fun createBinding(parent: ViewGroup, viewType: Int) = ItemStoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)

    override fun getViewHolder(binding: ItemStoryBinding) = StoriesViewHolder(binding)

    inner class StoriesViewHolder(private val binding: ItemStoryBinding) : GenericViewHolder<Movie>(binding.root) {
        override fun bind(data: Movie) {
            Glide.with(itemView.context).load(BuildConfig.BASE_POSTER_URL + data.posterPath).circleCrop().into(binding.storyPosterIV)
        }
    }
}