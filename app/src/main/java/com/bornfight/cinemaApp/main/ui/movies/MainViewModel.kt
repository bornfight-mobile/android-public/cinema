package com.bornfight.cinemaApp.main.ui.movies

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import com.bornfight.cinemaApp.main.model.Genre
import com.bornfight.cinemaApp.main.model.InTheaters
import com.bornfight.cinemaApp.main.model.RecommendedMovie
import com.bornfight.cinemaApp.main.model.UpcomingMovie
import com.bornfight.cinemaApp.main.repository.movies.MainRepo
import com.bornfight.common.mvvm.BaseViewModel

class MainViewModel @ViewModelInject constructor(private val mainRepo: MainRepo) : BaseViewModel() {

    val recommendedMovies: LiveData<List<RecommendedMovie>?> =
        observeData(
            localObserve = { mainRepo.observeRecommendedMovieList() },
            remoteFetch = { mainRepo.fetchRecommendedMovieList() }
        )

    val upcomingMovies: LiveData<List<UpcomingMovie>?> =
        observeData(
            localObserve = { mainRepo.observeUpcomingMovieList() },
            remoteFetch = { mainRepo.fetchUpcomingMovieList() }
        )

    val inTheaters: LiveData<List<InTheaters>?> =
        observeData(
            localObserve = { mainRepo.observeInTheatersList() },
            remoteFetch = { mainRepo.fetchInTheatersList() }
        )

    val genres: LiveData<List<Genre>?> =
        observeData(
            localObserve = { mainRepo.observeGenres() },
            remoteFetch = { mainRepo.fetchGenres() }
        )
}