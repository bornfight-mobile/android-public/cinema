package com.bornfight.cinemaApp.main.ui.movies.theaters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bornfight.cinemaApp.BuildConfig
import com.bornfight.cinemaApp.databinding.ItemInTheatersBinding
import com.bornfight.cinemaApp.main.model.Movie
import com.bornfight.utils.adapters.GenericAdapter2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class InTheatersAdapter : GenericAdapter2<Movie, ItemInTheatersBinding>() {

    override fun createBinding(parent: ViewGroup, viewType: Int) = ItemInTheatersBinding.inflate(LayoutInflater.from(parent.context), parent, false)

    override fun getViewHolder(binding: ItemInTheatersBinding) = InTheatersViewHolder(binding)

    inner class InTheatersViewHolder(private val binding: ItemInTheatersBinding) : GenericViewHolder<Movie>(binding.root) {
        override fun bind(data: Movie) {
            Glide.with(itemView.context)
                .load(BuildConfig.BASE_POSTER_URL + data.posterPath)
                .apply(RequestOptions().transform(RoundedCorners(8)))
                .into(binding.inTheatersIV)
        }
    }
}