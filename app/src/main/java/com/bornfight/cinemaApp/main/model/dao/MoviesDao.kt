package com.bornfight.cinemaApp.main.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bornfight.cinemaApp.main.model.Genre
import com.bornfight.cinemaApp.main.model.InTheaters
import com.bornfight.cinemaApp.main.model.RecommendedMovie
import com.bornfight.cinemaApp.main.model.UpcomingMovie
import kotlinx.coroutines.flow.Flow

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInTheatersList(inTheatresList: List<InTheaters>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecommendedMovieList(recommendedMovieList: List<RecommendedMovie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUpcomingMovieList(upcomingMovieList: List<UpcomingMovie>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenres(genres: List<Genre>)

    @Query("SELECT * FROM InTheaters")
    fun observeInTheatresList(): Flow<List<InTheaters>>

    @Query("SELECT * FROM RecommendedMovie")
    fun observeMovieList(): Flow<List<RecommendedMovie>>

    @Query("SELECT * FROM UpcomingMovie")
    fun observeUpcomingMovieList(): Flow<List<UpcomingMovie>>

    @Query("SELECT * FROM Genre")
    fun observeGenres(): Flow<List<Genre>>

}