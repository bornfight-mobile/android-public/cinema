package com.bornfight.cinemaApp.main.ui.movies.upcoming

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bornfight.cinemaApp.BuildConfig
import com.bornfight.cinemaApp.databinding.ItemUpcomingMovieBinding
import com.bornfight.cinemaApp.main.model.Movie
import com.bornfight.utils.GenreUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class UpcomingMovieFragment : Fragment() {

    private val upcomingMovie: Movie? by lazy { requireArguments().getParcelable(UPCOMING_MOVIE) }
    private var _binding: ItemUpcomingMovieBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = ItemUpcomingMovieBinding.inflate(inflater, container, false)
        Glide.with(binding.upcomingMovieIV.context)
            .load(BuildConfig.BASE_POSTER_URL + upcomingMovie?.posterPath)
            .apply(RequestOptions().transform(RoundedCorners(20))).into(binding.upcomingMovieIV)
        binding.titleTV.text = upcomingMovie?.title
        binding.genreTV.text = upcomingMovie?.genreIds?.get(0)?.let { GenreUtil.getGenreName(it) }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {

        private const val UPCOMING_MOVIE = "UPCOMING_MOVIE"

        fun newInstance(upcomingMovie: Movie): Fragment {
            return UpcomingMovieFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(UPCOMING_MOVIE, upcomingMovie)
                }
            }
        }
    }
}

