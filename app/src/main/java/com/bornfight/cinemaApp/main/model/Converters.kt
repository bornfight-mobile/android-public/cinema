package com.bornfight.cinemaApp.main.model

import androidx.room.TypeConverter
import com.google.gson.Gson

class Converters {

    @TypeConverter
    fun listToJson(value: List<Int>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Int>::class.java).toList()

    @TypeConverter
    fun objectListToJson(value: List<Movie>) = Gson().toJson(value)

    @TypeConverter
    fun jsonToObjectList(value: String) = Gson().fromJson(value, Array<Movie>::class.java).toList()

    @TypeConverter
    fun dateToJson(value: Dates) = Gson().toJson(value)

    @TypeConverter
    fun jsonToDate(value: String) = Gson().fromJson(value, Dates::class.java)

    @TypeConverter
    fun genreToJson(value: List<Genre>) = Gson().toJson(value)

    @TypeConverter
    fun jsonToGenre(value: String) = Gson().fromJson(value, Array<Genre>::class.java).toList()
}