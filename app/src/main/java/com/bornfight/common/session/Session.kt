package com.bornfight.common.session

/**
 * Created by tomislav on 16/12/2016.
 */
interface Session {

    val isLoggedIn: Boolean

    var token: String

    var userId: Long

    fun setLoggedIn(loggedIn: Boolean, userId: Long)

    fun logout()
}
