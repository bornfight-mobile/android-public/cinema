package com.bornfight.common.session

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Shared preferences which should not be backed up by Google when app reinstalled.
 * https://developer.android.com/guide/topics/data/autobackup
 */
@Singleton
class DevicePreferences @Inject constructor(@ApplicationContext context: Context) {

    private val mDevicePrefs: SharedPreferences =
        context.getSharedPreferences("device_preferences", Context.MODE_PRIVATE)

    var isFirebaseTokenSent: Boolean
        get() = mDevicePrefs.getBoolean("firebase_sent", false)
        set(sent) = mDevicePrefs.edit().putBoolean("firebase_sent", sent).apply()


//    fun setNotificationChannelEnabled(type: NotificationChannelsHelper.Channel, enabled: Boolean) {
//        mDevicePrefs.edit().putBoolean("notif_enabled_" + type.name, enabled).apply()
//    }
//
//    fun isNotificationChannelEnabled(type: NotificationChannelsHelper.Channel): Boolean {
//        return mDevicePrefs.getBoolean("notif_enabled_" + type.name, true)
//    }

}