package com.bornfight.common.session

import android.content.Context
import android.content.SharedPreferences
import com.bornfight.cinemaApp.BuildConfig
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by tomislav on 16/12/2016.
 */

@Singleton
class SessionPrefImpl @Inject
constructor(
    @ApplicationContext context: Context,
    private val devicePreferences: DevicePreferences
) : Session {
    private val mSharedPreferences: SharedPreferences =
        context.getSharedPreferences(BuildConfig.APPLICATION_ID + "_session", Context.MODE_PRIVATE)

    override val isLoggedIn: Boolean
        get() = mSharedPreferences.getBoolean("logged_in", false)

    override var userId: Long
        get() = mSharedPreferences.getLong("user_id", 0)
        set(userId) = mSharedPreferences.edit().putLong("user_id", userId).apply()

    override var token: String
        get() = mSharedPreferences.getString("session_token", "") ?: ""
        set(token) {
            mSharedPreferences.edit().putString("session_token", "Bearer $token").apply()
        }

    override fun setLoggedIn(loggedIn: Boolean, userId: Long) {
        mSharedPreferences.edit()
            .putBoolean("logged_in", loggedIn)
            .putLong("user_id", userId).apply()
    }

    override fun logout() {
        setLoggedIn(false, 0)

        // if user logged out, reset the Firebase token flag so it can
        // be sent again if user logs in back
        devicePreferences.isFirebaseTokenSent = false
        token = ""
    }

}
