package com.bornfight.common.hilt

import android.content.Context
import androidx.room.Room
import com.bornfight.common.data.database.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    /**
     * Database initialization.
     * Set database name and (optionally) provide a fallback strategy here.
     */
    @Singleton
    @Provides
    fun database(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "database.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

}