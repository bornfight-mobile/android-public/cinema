package com.bornfight.common.hilt

import com.bornfight.cinemaApp.main.repository.movies.MainRepo
import com.bornfight.cinemaApp.main.repository.movies.MainRepoImpl
import com.bornfight.demo.repository.news.NewsRepo
import com.bornfight.demo.repository.news.NewsRepoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@InstallIn(SingletonComponent::class)
@Module
abstract class RepoModule {

    @Binds
    abstract fun providesNewsRepo(newsRepo: NewsRepoImpl): NewsRepo

    @Binds
    abstract fun providesMainRepo(mainRepo: MainRepoImpl): MainRepo

}