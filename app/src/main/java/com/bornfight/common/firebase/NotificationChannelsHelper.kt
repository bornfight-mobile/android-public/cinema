package com.bornfight.common.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.bornfight.cinemaApp.R
import com.bornfight.common.firebase.NotificationChannelsHelper.VERSION

/**
 * Created by tomislav on 13/03/2018.
 *
 * This is a helper internal class which keeps configuration of notification channels on Android versions [Build.VERSION_CODES.O] and newer.
 * If creating a new channel, just update the [VERSION] so the new channels are registered to the system.
 */

object NotificationChannelsHelper {

    private const val PREFS_NAME = "NOTIF_CHANNELS"
    private const val CHANNELS_VERSION = "channels_version"

    /**
     * Version of channels setup. If added new channel, bump the version
     */
    private const val VERSION = 1

    /**
     * When adding new channel, update VERSION and getChannelName/Description methods.
     * Use [Channel.name] as channelId in [android.support.v4.app.NotificationCompat.Builder] constructor
     *
     * @see [com.bornfight.sdkname.firebase.MyFirebaseMessagingService.showNotification]
     */
    enum internal class Channel(private val channelName: Int, private val channelDesc: Int) {
        general(R.string.general, R.string.general_notif_info);

        fun getChannelName(context: Context): String = context.getString(channelName)
        fun getChannelDescription(context: Context): String = context.getString(channelDesc)
    }

    /**
     * Needs to be called before pushing the notification, best in [App.onCreate] method
     * @param context
     */
    fun initChannels(context: Context) {
        val sharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && VERSION > sharedPreferences.getInt(
                CHANNELS_VERSION, 0
            )
        ) {
            setupChannels(context)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun setupChannels(context: Context) {
        val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        for (channel in Channel.values()) {
            val notificationChannel = NotificationChannel(
                channel.name,
                channel.getChannelName(context),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.description = channel.getChannelDescription(context)
            mNotificationManager.createNotificationChannel(notificationChannel)
        }
    }

}
