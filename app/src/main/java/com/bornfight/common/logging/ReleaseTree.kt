package com.bornfight.common.logging

import android.util.Log
import com.google.firebase.crashlytics.FirebaseCrashlytics
import timber.log.Timber
import java.lang.Exception

class ReleaseTree : Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) return

        FirebaseCrashlytics.getInstance().apply {
            if (priority == Log.INFO) {
                log(message)
            } else {
                setCustomKey(KEY_PRIORITY, determinePriority(priority))
                setCustomKey(KEY_TAG, tag ?: MESSAGE_TAG_NULL)
                setCustomKey(KEY_MESSAGE, message)

                recordException(t ?: Exception(message))
            }
        }
    }

    private fun determinePriority(priority: Int): String {
        return when (priority) {
            LogType.WARN.value -> LogType.WARN.name
            LogType.ERROR.value -> LogType.ERROR.name
            LogType.ASSERT.value -> LogType.ASSERT.name
            else -> throw Exception("No such log level")
        }
    }

    private enum class LogType(val value: Int) {
        WARN(5),
        ERROR(6),
        ASSERT(7)
    }

    companion object {
        private const val KEY_PRIORITY = "priority"
        private const val KEY_TAG = "tag"
        private const val KEY_MESSAGE = "message"

        private const val MESSAGE_TAG_NULL = "unknown"
    }
}