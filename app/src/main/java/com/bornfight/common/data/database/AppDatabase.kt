package com.bornfight.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.bornfight.cinemaApp.main.model.*
import com.bornfight.cinemaApp.main.model.dao.MoviesDao
import com.bornfight.demo.model.Category
import com.bornfight.demo.model.News
import com.bornfight.demo.model.NewsRemoteKeys
import com.bornfight.demo.model.dao.CategoriesDao
import com.bornfight.demo.model.dao.NewsDao
import com.bornfight.demo.model.dao.NewsRemoteKeyDao

@Database(
    entities = [
        Category::class,
        News::class,
        NewsRemoteKeys::class,
        RecommendedMovie::class,
        UpcomingMovie::class,
        InTheaters::class,
        Genre::class],
    version = 20,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    // Provide each dao here, following the SearchSuggestionDao example.
    // ** Each entity used by any Dao, must be included within entities field of @Database annotation. **

    abstract fun categoriesDao(): CategoriesDao

    abstract fun newsDao(): NewsDao

    abstract fun newsRemoteKeysDao(): NewsRemoteKeyDao

    abstract fun moviesDao(): MoviesDao

}