package com.bornfight.common.data.retrofit

import com.bornfight.cinemaApp.main.model.Genres
import com.bornfight.cinemaApp.main.model.InTheatersResp
import com.bornfight.cinemaApp.main.model.RecommendedMoviesResp
import com.bornfight.cinemaApp.main.model.UpcomingMoviesResp
import com.bornfight.demo.model.Category
import com.bornfight.demo.model.News
import com.bornfight.demo.model.response.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    // Categories

    suspend fun getCategoryMock(categoryId: Long): Response<Category>

    suspend fun addCategoriesMock(categories: List<Category>)

    suspend fun addCategoryMock(category: Category)

    suspend fun updateCategoryMock(category: Category)

    suspend fun deleteCategoryMock(categoryId: Long)

    suspend fun deleteAllCategoriesMock()

    // News

    @GET("news/index")
    suspend fun getNews(
        @Query("isPromoted") isPromoted: Boolean?,
        @Query("showContest") showContest: Boolean?,
        @Query("perPage") limit: Int,
        @Query("page") page: Int
    ): Response<List<News>>

    // Movies


    @GET("movie/now_playing")
    suspend fun getMoviesInTheaters(
        @Query("api_key") apiKey: String
    ): InTheatersResp

    @GET("movie/popular")
    suspend fun getRecommendedMovies(
        @Query("api_key") apiKey: String
    ): RecommendedMoviesResp

    @GET("movie/upcoming")
    suspend fun getUpcomingMovies(
        @Query("api_key") apiKey: String
    ): UpcomingMoviesResp

    @GET("genre/movie/list")
    suspend fun getGenres(
        @Query("api_key") apiKey: String
    ): Genres
}