package com.bornfight.common.data.retrofit

import android.text.TextUtils
import android.util.Log
import com.bornfight.common.mvvm.model.Result
import com.bornfight.common.mvvm.model.ResultCodes
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import retrofit2.HttpException
import java.util.*


//Error responses
class GeneralError {
    @Expose
    @SerializedName("error", alternate = ["message"])
    var message: String = ""

    @Expose
    @SerializedName("errors")
    var errors: List<String> = emptyList()
}

class ValidationErrors {
    @Expose
    @SerializedName("errors")
    internal var errors: List<ValidationError> = ArrayList()

    fun getFieldErrors(fieldName: String): List<String> {
        for (ve in errors) {
            if (ve.field == fieldName) {
                val messages = ve.messages?.toList()
                return if (messages?.isNotEmpty() == true) {
                    messages
                } else {
                    listOf(ve.message)
                }
            }
        }
        return emptyList()
    }

    fun hasErrors(): Boolean {
        return errors.isNotEmpty()
    }

    fun getFieldError(fieldName: String, listDelimiter: CharSequence): String {
        for (ve in errors) {
            if (ve.field == fieldName) {
                return if (ve.messages?.isNotEmpty() == true) {
                    TextUtils.join(listDelimiter, ve.messages ?: emptyList<String>())
                } else {
                    ve.message
                }
            }
        }
        return ""
    }

    fun getErrors(): String {
        val stringBuilder = StringBuilder()
        for (ve in errors) {
            if (ve.messages?.isNotEmpty() == true) {
                stringBuilder.append(TextUtils.join(", ", ve.messages ?: emptyList<String>())).append("\n")
            } else {
                stringBuilder.append(ve.message).append("\n")
            }
        }
        return stringBuilder.toString()
    }

    fun setErrors(errors: List<ValidationError>) {
        this.errors = errors
    }

    inner class ValidationError {
        @SerializedName("field")
        internal var field = ""

        @SerializedName("messages")
        internal var messages: List<String>? = emptyList()

        @SerializedName("message")
        internal var message = ""
    }
}

/**
 * Convert HttpException into a meaningful Error to be rendered on View
 */
fun HttpException.toResultError(): Result.Error {
    val responseBody = response()
    val errorCode = responseBody?.code() ?: ResultCodes.UNDEFINED.code

    val errorBodyString = responseBody?.errorBody()?.string() ?: "{\"message\":\"Empty error body\"}"
    responseBody?.errorBody()?.close()
    try {
        val generalError = Gson().fromJson(errorBodyString, GeneralError::class.java)
        return Result.Error(generalError.message, this, errorCode)
    } catch (e1: JsonSyntaxException) {
        return try {
            val errorsList = Gson().fromJson(
                errorBodyString,
                object : TypeToken<List<ValidationErrors.ValidationError>>() {}.type
            ) as List<ValidationErrors.ValidationError>
            val validationErrors = ValidationErrors().apply { setErrors(errorsList) }
            Result.Error(validationErrors.getErrors(), this, errorCode, validationErrors)
        } catch (e2: JsonSyntaxException) {
            Log.e("ErrorMapper", "Exception ValidationError: " + e2.message, e2)
            Result.Error("Some error occurred.\nPlease try later.", this, errorCode)
        }
    }
}
