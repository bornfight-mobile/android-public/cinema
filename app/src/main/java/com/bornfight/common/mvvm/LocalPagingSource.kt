package com.bornfight.common.mvvm

import androidx.paging.PagingSource
import androidx.paging.PagingState

class LocalPagingSource<T : Any>(
    private val startPage: Int = 0,
    private val call: suspend (Int, Int) -> PagingSource<Int, T>
) : PagingSource<Int, T>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> {
        return try {
            val nextPageNumber = params.key ?: startPage
            val loadSize = params.loadSize

            call(loadSize, nextPageNumber).load(params)
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, T>): Int? {
        TODO("Not yet implemented")
    }
}