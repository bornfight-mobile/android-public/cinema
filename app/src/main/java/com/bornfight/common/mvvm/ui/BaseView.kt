package com.bornfight.common.mvvm.ui

import androidx.annotation.StringRes

interface BaseView {


    fun showProgressCircle(show: Boolean)

    fun showError(errorMessage: String)

    fun showError(stringResourceId: Int)

    fun showLoader(show: Boolean)

    fun showLoader(show: Boolean, cancelable: Boolean)

    fun showShortInfo(info: String)

    fun showShortInfo(stringResourceId: Int)

    fun showInfoDialog(title: String?, description: String?, buttonText: String?)

    fun showInfoDialog(
        @StringRes titleResourceId: Int,
        @StringRes descriptionResourceId: Int,
        @StringRes buttonText: Int
    )

    fun onLogout()

}