package com.bornfight.common.mvvm

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.bornfight.common.data.database.AppDatabase
import com.bornfight.common.mvvm.model.BaseRemoteKeys
import com.bornfight.common.mvvm.model.dao.BaseRemoteKeyDao
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

/**
 * Used for loading paginated data using remote+local data
 *
 * [remote] should sort data as in local source
 * */

@ExperimentalPagingApi
abstract class BasePageKeyedRemoteMediator<T : Any, R : BaseRemoteKeys>(
    private val startPage: Int = 0,
    private val db: AppDatabase,
    private val keysDao: BaseRemoteKeyDao<R>,
    private val remote: suspend (Int, Int) -> List<T>
) : RemoteMediator<Int, T>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, T>): MediatorResult {
        return try {
            // calculate the current page to load depending on the state
            val page = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: startPage
                }
                LoadType.PREPEND -> {
                    return MediatorResult.Success(true)
                }
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                        ?: throw InvalidObjectException("Result is empty")
                    remoteKeys.nextKey ?: return MediatorResult.Success(true)
                }
            }

            // load the list of items from API using calculated current page.
            // make sure the sort of the remote data and local data is the same!
            val response = remote(state.config.pageSize, page)


            // add custom logic, if you have some API metadata, you can use it as well
            val endOfPaginationReached = determineIfEndOfPaginationReached(response, state)

            db.withTransaction {
                // if refreshing, clear table and start over
                if (loadType == LoadType.REFRESH) {
                    clearDataFromDb()
                }

                val prevKey = if (page == startPage) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = response.map {
                    mapResponseToKeys(it, prevKey, nextKey)
                }

                insertDataToDb(response, keys)
            }

            MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }
    }

    open fun determineIfEndOfPaginationReached(
        response: List<T>,
        state: PagingState<Int, T>
    ): Boolean {
        return response.size < state.config.pageSize
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, T>): R? {
        return state.lastItemOrNull()?.let { news ->
            db.withTransaction { getRemoteKeys(news) }
        }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, T>): R? {
        return state.firstItemOrNull()?.let { news ->
            db.withTransaction { getRemoteKeys(news) }
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, T>): R? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.let {
                db.withTransaction { getRemoteKeys(it) }
            }
        }
    }

    private fun getRemoteKeys(item: T): R? {
        return keysDao.remoteKeysByItemId(getItemId(item))
    }

    abstract fun getItemId(item: T): Long

    abstract fun mapResponseToKeys(item: T, prevKey: Int?, nextKey: Int?): R

    abstract fun clearDataFromDb()

    abstract suspend fun insertDataToDb(data: List<T>, keys: List<R>)
}