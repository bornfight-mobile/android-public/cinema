package com.bornfight.common.mvvm.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class GenericPagingAdapter<T : Any>(diffCallback: BaseDiffUtil<T>) :
    PagingDataAdapter<T, GenericPagingAdapter.GenericViewHolder<T>>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<T> {
        return getViewHolder(
            LayoutInflater.from(parent.context).inflate(getLayoutId(viewType), parent, false),
            viewType
        )
    }

    override fun onBindViewHolder(holder: GenericViewHolder<T>, position: Int) {
        holder.bind(getItem(position) ?: return)
    }

    protected abstract fun getLayoutId(viewType: Int): Int

    protected abstract fun getViewHolder(view: View, viewType: Int): GenericViewHolder<T>

    abstract class GenericViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(data: T)
    }
}