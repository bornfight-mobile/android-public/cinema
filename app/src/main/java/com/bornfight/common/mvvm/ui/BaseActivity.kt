package com.bornfight.common.mvvm.ui

import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.bornfight.cinemaApp.R
import com.bornfight.common.session.DevicePreferences
import com.bornfight.common.session.SessionPrefImpl
import com.bornfight.utils.DialogUtil
import com.bornfight.utils.visibleIf
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), BaseView {

    @Inject
    lateinit var session: SessionPrefImpl

    @Inject
    protected lateinit var devicePrefs: DevicePreferences

    private lateinit var pd: Dialog

    private var toolbar: Toolbar? = null
    private var toolbarProgressCircle: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pd = MaterialAlertDialogBuilder(this).setTitle(R.string.please_wait).create()
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    fun checkPermissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
    }

    fun checkPermissionsGranted(vararg permissions: String): Boolean {
        for (permission in permissions) {
            if (!checkPermissionGranted(permission)) {
                return false
            }
        }
        return true
    }

    fun checkPermissionDenied(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////


    /*
        UI funcs:
     */
    override fun setContentView(@LayoutRes layoutResID: Int) {
        super.setContentView(layoutResID)
        initializeToolbar()
    }

    override fun setContentView(view: View?) {
        super.setContentView(view)
        initializeToolbar()
    }

    override fun setContentView(view: View?, params: ViewGroup.LayoutParams?) {
        super.setContentView(view, params)
        initializeToolbar()
    }

    private fun initializeToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbarProgressCircle = findViewById(R.id.toolbarProgressCircle)

        setSupportActionBar(toolbar)
    }

    override fun showError(errorMessage: String) {
        showShortInfo(errorMessage)
    }

    override fun showError(stringResourceId: Int) {
        showError(getString(stringResourceId))
    }

    override fun showShortInfo(info: String) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show()
    }

    override fun showShortInfo(stringResourceId: Int) {
        showShortInfo(getString(stringResourceId))
    }

    override fun showProgressCircle(show: Boolean) {
        toolbarProgressCircle?.visibleIf(show) ?: showLoader(show)
    }

    override fun showLoader(show: Boolean) {
        if (show) {
            pd.show()
        } else {
            pd.dismiss()
        }

        if (!show) {
            pd.setCancelable(true)
        }
    }

    override fun showLoader(show: Boolean, cancelable: Boolean) {
        pd.setCancelable(cancelable)
        showLoader(show)
    }

    override fun showInfoDialog(title: String?, description: String?, buttonText: String?) {
        DialogUtil.buildInfoDialog(
            this,
            title,
            description,
            buttonText
        ).show()
    }

    override fun showInfoDialog(
        @StringRes titleResourceId: Int,
        @StringRes descriptionResourceId: Int,
        @StringRes buttonText: Int
    ) {
        showInfoDialog(
            if (titleResourceId != 0) getString(titleResourceId) else null,
            if (descriptionResourceId != 0) getString(descriptionResourceId) else null,
            if (buttonText != 0) getString(buttonText) else null
        )
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    override fun onLogout() {
        // clear the flag for sending the Firebase token
        devicePrefs.isFirebaseTokenSent = false

        //TODO: open login page if logged out
        /*
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK;
        startActivity(intent)
        supportFinishAfterTransition()
        */
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
}