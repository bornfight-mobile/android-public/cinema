package com.bornfight.common.mvvm.model

import com.bornfight.cinemaApp.R
import com.bornfight.common.data.retrofit.ValidationErrors
import com.bornfight.common.mvvm.ui.BaseView

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out T> {

    object Loading : Result<Nothing>()
    data class Success<out T>(val data: T) : Result<T>()

    /**
     * @param error Error message to be shown
     * @param exception Originating exception
     * @param errorCode[ResultCodes] Error code from [ResultCodes]
     * @param validationErrors If server returns multiple errors per each field (login forms etc.)
     */
    data class Error(
        val error: String = "Some error occurred.\nPlease try later.",
        val exception: Throwable = Exception(error),
        val errorCode: Int = ResultCodes.UNDEFINED.code,
        val validationErrors: ValidationErrors? = null
    ) : Result<Nothing>()


    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$error]"
            Loading -> "Loading"
        }
    }

    val success
        get() = this is Success && data != null

    val dataOrNull: T?
        get() {
            return if (this is Success) {
                data
            } else {
                null
            }
        }
}


/**
 * Define how you want to render a specific error here.
 */
fun <T> Result<T>.render(view: BaseView, onSuccess: ((T) -> Unit)? = null, onError: ((Result.Error) -> Unit)? = null) {
    when (this) {
        is Result.Loading -> view.showProgressCircle(true)
        is Result.Success -> onSuccess?.invoke(this.data) ?: view.showProgressCircle(false)
        is Result.Error -> onError?.invoke(this) ?: run {
            view.showProgressCircle(false)

            // additional handling of specific states
            when (errorCode) {
                ResultCodes.NO_NETWORK.code -> view.showError(R.string.network_error)
                ResultCodes.UNAUTHORIZED.code -> view.onLogout()
                else -> view.showError(error)
            }
        }
    }


}

enum class ResultCodes(val code: Int) {
    UNDEFINED(0),
    NO_NETWORK(1),
    UNAUTHORIZED(401)
}