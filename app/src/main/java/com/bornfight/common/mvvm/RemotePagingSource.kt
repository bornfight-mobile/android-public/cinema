package com.bornfight.common.mvvm

import androidx.paging.PagingSource
import androidx.paging.PagingState

class RemotePagingSource<T : Any>(
    private val startPage: Int = 0,
    private val call: suspend (Int, Int) -> List<T>
) : PagingSource<Int, T>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> {
        return try {
            val nextPage = params.key ?: startPage
            val loadSize = params.loadSize

            val response = call(loadSize, nextPage)

            LoadResult.Page(
                data = call(loadSize, nextPage),
                prevKey = if (nextPage == 1) null else nextPage - 1,
                nextKey = if (response.size < params.loadSize) null else nextPage + 1
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, T>): Int? {
        TODO("Not yet implemented")
    }
}