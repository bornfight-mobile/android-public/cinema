package com.bornfight.common.mvvm

import android.util.Log
import com.bornfight.common.data.retrofit.toResultError
import com.bornfight.common.mvvm.model.Result
import com.bornfight.common.mvvm.model.ResultCodes
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException


private const val TAG = "BaseRepo"

abstract class BaseRepo {

    /**
     * Fetches the data from remote and saves to database
     * */
    suspend fun <T> fetchData(
        remote: suspend () -> T,
        save: (suspend (T) -> Unit)? = null
    ): Flow<Result<T>> {
        return flow {
            emit(Result.Loading)
            try {
                val data = remote()
                save?.invoke(data)
                emit(Result.Success(data))
            } catch (e: Exception) {
                Log.e(TAG, "fetchData", e)
                emit(processRemoteException(e))
            }
        }.catch { exception ->
            emit(processRemoteException(exception))
        }
    }

    private fun processRemoteException(e: Throwable): Result.Error {
        val message = e.localizedMessage ?: "Network error"
        return when (e) {
            is IOException -> Result.Error(message, e, ResultCodes.NO_NETWORK.code)
            is HttpException -> e.toResultError()
            else -> Result.Error(message, e)
        }
    }
}