package com.bornfight.common.mvvm.model.dao

import com.bornfight.common.mvvm.model.BaseRemoteKeys

interface BaseRemoteKeyDao<T : BaseRemoteKeys> {

    fun insertAll(remoteKey: List<@JvmSuppressWildcards T>)

    fun remoteKeysByItemId(itemId: Long): @JvmSuppressWildcards T?

    fun clearRemoteKeys()

}