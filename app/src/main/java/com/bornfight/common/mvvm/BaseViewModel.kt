package com.bornfight.common.mvvm

import androidx.lifecycle.*
import com.bornfight.common.mvvm.model.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

open class BaseViewModel : ViewModel() {

    private val _generalEvents: MutableLiveData<Event<Result<*>>> = MutableLiveData()
    val generalEvents: LiveData<Event<Result<*>>> = _generalEvents

    fun <T> observeData(
        localObserve: () -> Flow<T>,
        remoteFetch: suspend () -> Flow<Result<T>>,
        triggerRefresh: MutableLiveData<Boolean> = MutableLiveData(true),
        progressEvents: MutableLiveData<Event<Result<T>>>? = null
    ): LiveData<T> {
        return triggerRefresh.switchMap { refresh ->
            if (refresh) {
                triggerRefresh.value = false
                viewModelScope.launch {
                    remoteFetch().collect { result ->
                        progressEvents?.apply {
                            value = Event(result)
                        } ?: kotlin.run {
                            _generalEvents.value = Event(result)
                        }
                    }
                }
            }
            localObserve().asLiveData().distinctUntilChanged()
        }
    }
}