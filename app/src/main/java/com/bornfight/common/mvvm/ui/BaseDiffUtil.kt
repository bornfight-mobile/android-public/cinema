package com.bornfight.common.mvvm.ui

import androidx.recyclerview.widget.DiffUtil

class BaseDiffUtil<T>(
    private val areItemsSame: (T, T) -> Boolean,
    private val areContentsSame: (T, T) -> Boolean = { old, new -> old == new }
) : DiffUtil.ItemCallback<T>() {

    override fun areItemsTheSame(oldItem: T, newItem: T) = this.areItemsSame(oldItem, newItem)

    override fun areContentsTheSame(oldItem: T, newItem: T) = this.areContentsSame(oldItem, newItem)
}