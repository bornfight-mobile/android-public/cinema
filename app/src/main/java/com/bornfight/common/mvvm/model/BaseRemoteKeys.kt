package com.bornfight.common.mvvm.model

interface BaseRemoteKeys {
    val prevKey: Int?
    val nextKey: Int?
}