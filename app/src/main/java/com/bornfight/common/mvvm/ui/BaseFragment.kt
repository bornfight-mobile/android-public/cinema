package com.bornfight.common.mvvm.ui

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment(), BaseView {

    //Added a check, tests dont pass if there is no check(EmptyFragmentActivity cant be cast to BaseActivity)
    var baseActivity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) baseActivity = context
    }

    override fun onDetach() {
        baseActivity = null
        super.onDetach()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    override fun showError(errorMessage: String) {
        baseActivity?.showShortInfo(errorMessage)
    }

    override fun showError(stringResourceId: Int) {
        baseActivity?.showError(stringResourceId)
    }

    override fun showShortInfo(info: String) {
        baseActivity?.showShortInfo(info)
    }

    override fun showShortInfo(stringResourceId: Int) {
        baseActivity?.showShortInfo(stringResourceId)
    }

    override fun showProgressCircle(show: Boolean) {
        baseActivity?.showProgressCircle(show)
    }

    override fun showLoader(show: Boolean) {
        baseActivity?.showLoader(show)
    }

    override fun showLoader(show: Boolean, cancelable: Boolean) {
        baseActivity?.showLoader(show, cancelable)
    }

    override fun showInfoDialog(title: String?, description: String?, buttonText: String?) {
        baseActivity?.showInfoDialog(title, description, buttonText)
    }

    override fun showInfoDialog(
        @StringRes titleResourceId: Int,
        @StringRes descriptionResourceId: Int,
        @StringRes buttonText: Int
    ) {
        baseActivity?.showInfoDialog(titleResourceId, descriptionResourceId, buttonText)
    }

    override fun onLogout() {
        baseActivity?.onLogout()
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////


    protected fun checkPermissionGranted(permission: String): Boolean {
        return baseActivity?.checkPermissionGranted(permission) == true
    }

    protected fun checkPermissionsGranted(vararg permissions: String): Boolean {
        return baseActivity?.checkPermissionsGranted(*permissions) == true
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
}